from django.shortcuts import render, redirect
from django import forms
from django.http import HttpResponse, Http404
from django.template import loader
from django.utils import timezone

from rest_framework import viewsets
from serializers import PostSerializer, GroupSerializer

from blogging.models import Post, Category
from blogging.forms import PostModelForm

def list_view(request):
    published_posts = Post.objects.exclude(published_date__exact=None)
    posts = published_posts.order_by('-published_date')
    context = {'posts': posts}
    return render(request, 'blogging/list.html', context)

def detail_view(request, post_id):
    published_posts = Post.objects.exclude(published_date__exact=None)
    try:
        post = published_posts.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404
    context = {'post': post}
    return render(request, 'blogging/detail.html', context)

def add_view(request):
    if request.method == 'POST':
        form = PostModelForm(request.POST)
        if form.is_valid():
            post_instance = form.save(commit=False)
            post_instance.published_date = timezone.now()
            # Prevent Cross-site scripting attacks
            post_instance.text.replace('<', '&lt;').replace('>', '&gt;')
            post_instance.save()
            return redirect('/')
    else:
        form = PostModelForm()

    context = {'form': form}
    return render(request, 'blogging/post_form.html', context)

def post_api_view(request):
    pass




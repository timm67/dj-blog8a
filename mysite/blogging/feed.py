from django.contrib.syndication.views import Feed
from django.urls import reverse
from .models import Post

class LatestPostsFeed(Feed):
    title = "Django blog site news"
    link = "/postfeed/"
    description = "Latest changes to the blog as RSS feed"

    def items(self):
        published_posts = Post.objects.exclude(published_date__exact=None)
        return published_posts.order_by('-published_date')[:5]

    def item_title(self, item):
        return item.title

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return reverse('post', args=[item.pk])
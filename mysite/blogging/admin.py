from django import forms
from django.contrib import admin
from blogging.models import Post, Category

class CategoryForm(forms.ModelForm):

    class Meta:
        fields = ['name', 'description']
        model = Category

class CategoryAdmin(admin.ModelAdmin):
    exclude = ['posts']
    form = CategoryForm

class CategoryInline(admin.TabularInline):
    model = Category

class PostAdmin(admin.ModelAdmin):
    inlines = [
        CategoryInline,
    ]

# Register your models here.
admin.site.register(Post)
admin.site.register(Category)
